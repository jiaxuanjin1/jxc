package com.atguigu.jxc.controller;/*
 * @author: XueYouPeng
 * @time: 23.8.7 上午 10:06
 */

import com.atguigu.jxc.service.UnitService;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.Map;

@RestController
@RequestMapping("/unit")
public class UnitController {

    @Autowired
    private UnitService unitService;

    /*
     * 商品管理
     * @author: XueYouPeng
     * @time: 23.8.7 上午 10:14
     */
    @PostMapping(value = "/list")
    @ResponseBody
    @RequiresPermissions(value = "供应商管理")
    public Map<String , Object> unitList(){
        return unitService.findUnitList();
    }
}
