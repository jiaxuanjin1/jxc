package com.atguigu.jxc.controller;

import com.atguigu.jxc.domain.ServiceVO;
import com.atguigu.jxc.service.GoodsTypeService;
import org.apache.shiro.authz.annotation.Logical;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;

/**
 * @description 商品类别控制器
 */
@RestController
@RequestMapping("/goodsType")
public class GoodsTypeController {

    @Autowired
    private GoodsTypeService goodsTypeService;

    /**
     * 查询所有商品类别
     * @return easyui要求的JSON格式字符串
     */
    @PostMapping("/loadGoodsType")
    @RequiresPermissions(value={"商品管理","进货入库","退货出库","销售出库","客户退货","当前库存查询","商品报损","商品报溢","商品采购统计"},logical = Logical.OR)
    public ArrayList<Object> loadGoodsType() {
        return goodsTypeService.loadGoodsType();
    }

    /*
     * 新增分类
     * @author: XueYouPeng
     * @time: 23.8.7 上午 11:42
     */
    @PostMapping(value = "/save")
    @RequiresPermissions(value = "商品分类")
    public ServiceVO save(@RequestParam(value = "goodsTypeName") String goodsTypeName ,
                          @RequestParam(value = "pId") Integer pId ,
                          @RequestParam(value = "goodsTypeState" , required = false) Integer goodsTypeState){
        if (goodsTypeState == null){
            goodsTypeState = 0;
        }
        return goodsTypeService.save(goodsTypeName , pId , goodsTypeState);
    }


    @PostMapping(value = "/delete")
    public ServiceVO delete(@RequestParam(value = "goodsTypeId") Integer goodsTypeId){
        return goodsTypeService.delete(goodsTypeId);
    }
}
