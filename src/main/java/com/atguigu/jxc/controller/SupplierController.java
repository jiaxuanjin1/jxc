package com.atguigu.jxc.controller;/*
 * @author: XueYouPeng
 * @time: 23.8.5 下午 10:33
 */

import com.atguigu.jxc.domain.ServiceVO;
import com.atguigu.jxc.entity.Supplier;
import com.atguigu.jxc.service.SupplierService;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.Map;

@RestController
@RequestMapping(value = "/supplier")
public class SupplierController {

    @Autowired
    private SupplierService supplierService;

    /*
     * 查询供应商 分页
     * @author: XueYouPeng
     * @time: 23.8.5 下午 10:41
     */
    @PostMapping(value = "/list")
    @RequiresPermissions(value = "供应商管理")
    public Map<String , Object> supplierPage(@RequestParam("page") Integer page ,
                                             @RequestParam(value = "rows") Integer rows ,
                                             @RequestParam(value = "supplierName" , required = false) String supplierName){
        return supplierService.listSupplierPage(page , rows , supplierName);
    }

    /*
     * 供应商添加或修改用户信息
     * @author: XueYouPeng
     * @time: 23.8.6 下午 6:52
     */
     @PostMapping(value = "/save")
     @ResponseBody
     @RequiresPermissions(value = "供应商管理")
     public ServiceVO saveOrUpdate( Supplier supplier){
         return supplierService.saveOrUpdate(supplier);
     }

     /*
      * 删除供应商（支持批量删除）
      * @author: XueYouPeng
      * @time: 23.8.6 下午 8:51
      */
      @PostMapping(value = "/delete")
      @RequiresPermissions(value = "供应商管理")
      @ResponseBody
      public ServiceVO delete(String ids){
          return supplierService.delete(ids);
      }
}
