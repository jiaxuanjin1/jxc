package com.atguigu.jxc.controller;/*
 * @author: XueYouPeng
 * @time: 23.8.8 上午 9:01
 */

import com.atguigu.jxc.domain.ServiceVO;
import com.atguigu.jxc.entity.OverflowList;
import com.atguigu.jxc.entity.User;
import com.atguigu.jxc.service.OverflowListGoodsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpSession;
import java.util.Map;

@RestController
@RequestMapping(value = "/overflowListGoods")
public class OverflowListGoodsController {

    @Autowired
    private OverflowListGoodsService overflowListGoodsService;

    /*
     * 新增报溢单
     * @author: XueYouPeng
     * @time: 23.8.8 上午 9:08
     */
    @PostMapping(value = "/save")
    @ResponseBody
    public ServiceVO save(OverflowList overflowList , String overflowListGoodsStr , HttpSession session){

        return overflowListGoodsService.save(overflowList, overflowListGoodsStr,session);
    }

    /*
     * 报溢单查询
     * @author: XueYouPeng
     * @time: 23.8.8 上午 10:28
     */
    @PostMapping(value = "/list")
    public Map<String , Object> list(String sTime , String eTime, HttpSession session){
        return overflowListGoodsService.list(sTime , eTime,session);
    }

    /*
     * 查询报溢价单商品信息
     * @author: XueYouPeng
     * @time: 23.8.8 上午 10:45
     */
    @PostMapping(value = "/goodsList")
    public Map<String , Object> goodsList(Integer overflowListId){
        return overflowListGoodsService.goodsList(overflowListId);
    }
}


























