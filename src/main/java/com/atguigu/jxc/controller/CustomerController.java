package com.atguigu.jxc.controller;
/* 客户的controller
 * @author: XueYouPeng
 * @time: 23.8.7 上午 8:19
 */

import com.atguigu.jxc.domain.ServiceVO;
import com.atguigu.jxc.entity.Customer;
import com.atguigu.jxc.service.CustomerService;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.Map;

@RestController
@RequestMapping(value = "customer")
public class CustomerController {


    @Autowired
    private CustomerService customerService;


    /*
     * 客户列表分页（名称模糊查询）
     * @author: XueYouPeng
     * @time: 23.8.7 上午 8:28
     */
    @PostMapping(value = "/list")
    @ResponseBody
    @RequiresPermissions(value = "客户管理")
    public Map<String , Object> customerPage(
            Integer page, Integer rows,
            @RequestParam(value = "customerName" , required = false)String customerName){

        return customerService.listCustomerPage(page,rows,customerName);
    }

    /*
     * 客户添加或修改
     * @author: XueYouPeng
     * @time: 23.8.7 上午 9:06
     */
     @PostMapping(value = "/save")
     @ResponseBody
     @RequiresPermissions(value = "客户管理")
     public ServiceVO saveOrUpdate(Customer customer){
         return customerService.saveOrUpdate(customer);
     }


     /*
      * 客户删除（支持批量删除）
      * @author: XueYouPeng
      * @time: 23.8.7 上午 9:33
      */
      @PostMapping(value = "/delete")
      @ResponseBody
      @RequiresPermissions(value = "客户管理")
      public ServiceVO delete(String ids){
          return customerService.delete(ids);
      }

}
