package com.atguigu.jxc.service;/*
 * @author: XueYouPeng
 * @time: 23.8.7 下午 7:21
 */

import com.atguigu.jxc.domain.ServiceVO;
import com.atguigu.jxc.entity.DamageList;


import javax.servlet.http.HttpSession;
import java.util.Map;

public interface DamageListService {
    ServiceVO save(DamageList damageList, String damageListGoodsStr, HttpSession session);

    Map<String, Object> list(String sTime, String eTime, HttpSession session);

    Map<String, Object> goodsList(Integer damageListId);
}
