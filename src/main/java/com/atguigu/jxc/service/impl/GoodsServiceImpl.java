package com.atguigu.jxc.service.impl;

import com.atguigu.jxc.dao.GoodsDao;
import com.atguigu.jxc.domain.ErrorCode;
import com.atguigu.jxc.domain.ServiceVO;
import com.atguigu.jxc.domain.SuccessCode;
import com.atguigu.jxc.entity.Goods;
import com.atguigu.jxc.entity.Log;
import com.atguigu.jxc.service.*;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * @description
 */
@Service
@Slf4j
public class GoodsServiceImpl implements GoodsService {

    @Autowired
    private GoodsDao goodsDao;



    @Override
    public ServiceVO getCode() {

        // 获取当前商品最大编码
        String code = goodsDao.getMaxCode();

        // 在现有编码上加1
        Integer intCode = Integer.parseInt(code) + 1;

        // 将编码重新格式化为4位数字符串形式
        String unitCode = intCode.toString();

        for(int i = 4;i > intCode.toString().length();i--){

            unitCode = "0"+unitCode;

        }
        return new ServiceVO<>(SuccessCode.SUCCESS_CODE, SuccessCode.SUCCESS_MESS, unitCode);
    }

    @Override
    public Map<String, Object> listInventory(Integer page, Integer rows, String codeOrName, Integer goodsTypeId) {

        Map<String,Object> map = new HashMap<>();


        int offSet = (page - 1) * rows;
        Goods goods=new Goods();

        //此时new出来的goods对象是空的 所以要先把前端传过来的typeId穿进去
        goods.setGoodsTypeId(goodsTypeId);

        //判断传过来的codeOrName是code还是name
        if (codeOrName != null && !codeOrName.isEmpty()) {
            if (isNumeric(codeOrName)) {
                goods.setGoodsCode(codeOrName);
            } else {
                goods.setGoodsName(codeOrName);
            }
        }

        List<Goods> goodsList = goodsDao.findGoodsBycodeOrNameAndgoodsTypeId(offSet, rows, goods);

        //补全saleTotle字段
        for (Goods goods_one : goodsList){
            Integer goodsId = goods_one.getGoodsId();
            log.info("goodsId得到了{}" + goodsId);
            //查询出总的销售数量
            Integer saleNum = goodsDao.findGoodsSaleByGoodsId(goodsId);
            saleNum = saleNum == null ? 0 : saleNum;
            //查询出顾客退货总量
            Integer retunNum = goodsDao.findGoodsRetunByGoodsId(goodsId);
            retunNum = retunNum == null ? 0 :retunNum;

            goods_one.setSaleTotal(saleNum - retunNum);
        }


        map.put("rows", goodsList);
        map.put("total", goodsDao.countInventory(goods, goodsTypeId));

        return map;
    }

    @Override
    public Map<String, Object> goodsList(Integer page, Integer rows, String goodsName, Integer goodsTypeId) {

        Map<String,Object> map = new HashMap<>();

        int start = (page - 1) * rows;

        //查询商品数据
        List<Goods> goodsList = goodsDao.findGoodsListByTypeOrName(start, rows, goodsName,goodsTypeId);
        //total
        int total = goodsList.size();


        map.put("rows", goodsList);
        map.put("total", total);

        return map;
    }

    @Override
    public ServiceVO saveOrUpdate(Goods goods, Integer goodsTypeId ) {

        System.out.println(goodsTypeId);

        //根据id判断 是新增还是修改操作
        if (goods.getGoodsId() == null){
            if (goods.getInventoryQuantity() == null){
                goods.setInventoryQuantity(1000);
                goods.setLastPurchasingPrice(30);
                goods.setState(0);
                goods.setGoodsTypeId(goodsTypeId);
            }

            //执行新增
            goodsDao.saveGoods(goods);
        }else {
            //执行修改
            goodsDao.updateGoods(goods);
        }

        return new ServiceVO(SuccessCode.SUCCESS_CODE , SuccessCode.SUCCESS_MESS);

    }

    @Override
    public ServiceVO deleteGoods(Integer goodsId) {

        goodsDao.deleteGoods(goodsId);
        return new ServiceVO<>(SuccessCode.SUCCESS_CODE, SuccessCode.SUCCESS_MESS);
    }

    @Override
    public Map<String, Object> noInventoryList(Integer page, Integer rows, String nameOrCode) {

        HashMap<String, Object> map = new HashMap<>();

        int start = (page - 1) * rows;
        Goods goods=new Goods();

        //判断传过来的codeOrName是code还是name
        if (nameOrCode != null && !nameOrCode.isEmpty()) {
            if (isNumeric(nameOrCode)) {
                goods.setGoodsCode(nameOrCode);
            } else {
                goods.setGoodsName(nameOrCode);
            }
        }

        List<Goods> goodsList = goodsDao.findNoInventoryList(start , rows ,goods);
        int total = goodsList.size();

        map.put("total" , total);
        map.put("rows" , goodsList);

        return map;
    }

    @Override
    public Map<String, Object> haveInventoryList(Integer page, Integer rows, String nameOrCode) {

        HashMap<String, Object> map = new HashMap<>();

        int start = (page - 1) * rows;
        Goods goods=new Goods();

        //判断传过来的codeOrName是code还是name
        if (nameOrCode != null && !nameOrCode.isEmpty()) {
            if (isNumeric(nameOrCode)) {
                goods.setGoodsCode(nameOrCode);
            } else {
                goods.setGoodsName(nameOrCode);
            }
        }

        List<Goods> goodsList = goodsDao.findHaveInventoryList(start , rows ,goods);
        int total = goodsList.size();

        map.put("total" , total);
        map.put("rows" , goodsList);

        return map;
    }

    @Override
    public ServiceVO saveorUpdateStock(Integer goodsId, Integer inventoryQuantity, double purchasingPrice) {

        //执行新增或修改
        goodsDao.saveOrUpdateStock(goodsId , inventoryQuantity , purchasingPrice);

        return new ServiceVO(SuccessCode.SUCCESS_CODE , SuccessCode.SUCCESS_MESS);

    }

    @Override
    public ServiceVO deleteStock(Integer goodsId) {

        Goods goods = goodsDao.findGoodsByGoodsId(goodsId);

        if (goods.getState() == 0){

            goodsDao.deleteGoodsStock(goodsId);
        }else {
            return new ServiceVO(ErrorCode.STATUS_NOTTWO_CODE , ErrorCode.STATUS_NOTTWO_MESS);
        }

        return new ServiceVO<>(SuccessCode.SUCCESS_CODE, SuccessCode.SUCCESS_MESS);
    }

    @Override
    public Map<String, Object> listAlarm() {

        Map<String, Object> map = new HashMap<>();

        //查询当前库存量 小于 库存下限的商品信息
        List<Goods> goodsList = goodsDao.findListAlarm();

        //封装数据
        map.put("rows" , goodsList);

        return map;
    }

    private boolean isNumeric(String str) {
        return str.matches("\\d+");
    }

}
