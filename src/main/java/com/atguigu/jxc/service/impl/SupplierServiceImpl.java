package com.atguigu.jxc.service.impl;/*
 * @author: XueYouPeng
 * @time: 23.8.5 下午 10:34
 */

import com.atguigu.jxc.dao.SupplierDao;
import com.atguigu.jxc.domain.ErrorCode;
import com.atguigu.jxc.domain.ServiceVO;
import com.atguigu.jxc.domain.SuccessCode;
import com.atguigu.jxc.entity.Supplier;
import com.atguigu.jxc.service.SupplierService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.*;

@Service
@Slf4j
public class SupplierServiceImpl implements SupplierService {

    @Autowired
    private SupplierDao supplierDao ;

    @Override
    public Map<String, Object> listSupplierPage(Integer page, Integer rows, String supplierName) {

        Map<String, Object> map = new HashMap<>();

        int start = (page - 1) * rows ;


        //查询供应商的数据
        List<Supplier> supplierList = supplierDao.findSuppliersBySupName(start , rows , supplierName);

        //查询total  不用写sql  遍历supplierList 拿到每一个supplier 看有几个 拿到长度
        int total = supplierList.size();

        //封装数据
        map.put("total" , total);
        map.put("rows" , supplierList);

        return map;
    }

    @Override
    public ServiceVO saveOrUpdate(Supplier supplier) {

        //根据id判断 是新增还是修改操作
        if (supplier.getSupplierId() == null){

            //新增操作 先判断是否已经存在此供应商
            Supplier existSupplier = supplierDao.findSupplierByname(supplier.getSupplierName());

            //如果不为空 则此供应商已存在
            if (existSupplier != null){
                return new ServiceVO(ErrorCode.SUPPLIER_EXIST_CODE , ErrorCode.SUPPLIER_EXIST_MESS);
            }

            //否则 就执行新增操作
            supplierDao.saveSupplier(supplier);
        }else {
            //执行更新操作
            supplierDao.updateSupplier(supplier);
        }
        return new ServiceVO(SuccessCode.SUCCESS_CODE , SuccessCode.SUCCESS_MESS);
    }

    @Override
    public ServiceVO delete(String ids) {

        //拿到前端传过来的id 存贷集合里
        List<String> idList = Arrays.asList(ids.split(","));

        //执行删除操作
        supplierDao.deleteByIds(idList);

        return new ServiceVO<>(SuccessCode.SUCCESS_CODE, SuccessCode.SUCCESS_MESS);
    }
}
