package com.atguigu.jxc.service;/*
 * @author: XueYouPeng
 * @time: 23.8.7 上午 8:22
 */

import com.atguigu.jxc.domain.ServiceVO;
import com.atguigu.jxc.entity.Customer;

import java.util.Map;

public interface CustomerService {
    Map<String, Object> listCustomerPage(Integer page, Integer rows, String customerName);

    ServiceVO saveOrUpdate(Customer customer);

    ServiceVO delete(String ids);
}
