package com.atguigu.jxc.service;/*
 * @author: XueYouPeng
 * @time: 23.8.7 上午 10:07
 */

import java.util.Map;

public interface UnitService {
    Map<String, Object> findUnitList();
}
