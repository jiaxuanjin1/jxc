package com.atguigu.jxc.dao;/*
 * @author: XueYouPeng
 * @time: 23.8.7 上午 10:08
 */

import com.atguigu.jxc.entity.Unit;

import java.util.List;

public interface UnitDao {

    List<Unit> findUnit();
}
