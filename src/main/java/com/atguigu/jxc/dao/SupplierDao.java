package com.atguigu.jxc.dao;/*
 * @author: XueYouPeng
 * @time: 23.8.5 下午 10:35
 */

import com.atguigu.jxc.entity.Supplier;
import org.apache.ibatis.annotations.Param;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.ArrayList;
import java.util.List;

public interface SupplierDao {
    //查询分页供应商数据
    List<Supplier> findSuppliersBySupName(int start, Integer rows, String supplierName);

    //通过供应商名称查询
    Supplier findSupplierByname(String supplierName);

    //新增供应商
    void saveSupplier(Supplier supplier);

    void updateSupplier( Supplier supplier);

    void deleteByIds(@Param("idList") List<String> idList);

    //void deleteByIds(String ids_one);




    //Integer countSuulier(Integer supplierId);
}
