package com.atguigu.jxc.dao;/*
 * @author: XueYouPeng
 * @time: 23.8.8 上午 9:03
 */

import com.atguigu.jxc.entity.OverflowList;
import com.atguigu.jxc.entity.OverflowListGoods;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface OverflowListGoodsDao {

    void saveOverFlowList(OverflowList overflowList);

    OverflowList findOverFlow(String overflowNumber);

    void saveOverFlowListValue(@Param(value = "overflowListGoodsList") List<OverflowListGoods> overflowListGoodsList);

    List<OverflowList> findList(@Param(value = "sTime") String sTime, @Param(value = "eTime") String eTime );

    List<OverflowListGoods> findGoodsList(@Param(value = "overflowListId") Integer overflowListId);
}
