package com.atguigu.jxc.dao;/*
 * @author: XueYouPeng
 * @time: 23.8.7 上午 8:23
 */

import com.atguigu.jxc.entity.Customer;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface CustomerDao {
    List<Customer> findCustomerByName(int start, Integer rows, String customerName);


    Customer findEXCustomerByName(String customerName);

    void saveCustomer(Customer customer);

    void updateCustomer(Customer customer);

    void deleteByIds(@Param("idList") List<String> idList);
}
