package com.atguigu.jxc.dao;

import com.atguigu.jxc.entity.Goods;
import com.atguigu.jxc.entity.GoodsType;

import java.util.List;

/**
 * @description 商品类别
 */
public interface GoodsTypeDao {

    List<GoodsType> getAllGoodsTypeByParentId(Integer pId);

    Integer updateGoodsTypeState(GoodsType parentGoodsType);


    GoodsType findGoodsTypeByName(String goodsTypeName);

    void saveGoodsType(String goodsTypeName, Integer pId , Integer goodsTypeState);

    void deleteById(Integer goodsTypeId);

}
