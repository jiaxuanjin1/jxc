package com.atguigu.jxc.dao;/*
 * @author: XueYouPeng
 * @time: 23.8.7 下午 7:22
 */

import com.atguigu.jxc.entity.DamageList;
import com.atguigu.jxc.entity.DamageListGoods;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface DamageListDao {


    void saveDamageList(DamageList damageList);

    DamageList findDamage(String damageNumber);

    void saveDamageListValue(@Param(value = "damageListGoodsList") List<DamageListGoods> damageListGoodsList);

    List<DamageList> findList(@Param(value = "sTime") String sTime, @Param(value = "eTime") String eTime );

    List<DamageListGoods> findGoodsList(@Param(value = "damageListId") Integer damageListId);
}
