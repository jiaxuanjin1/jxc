package com.atguigu.jxc.dao;

import com.atguigu.jxc.entity.Goods;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * @description 商品信息
 */
public interface GoodsDao {


    String getMaxCode();


    Object countInventory(Goods goods, Integer goodsTypeId);

    List<Goods> findGoodsBycodeOrNameAndgoodsTypeId(int offSet, Integer rows, Goods goods);

    Integer findGoodsSaleByGoodsId(Integer goodsId);

    Integer findGoodsRetunByGoodsId(Integer goodsId);

    List<Goods> findGoodsListByTypeOrName(int start, Integer rows, String goodsName, Integer goodsTypeId);

    void saveGoods(Goods goods  );

    void updateGoods(Goods goods );

    void deleteGoods(Integer goodsId);

    List<Goods> findNoInventoryList(int start, Integer rows, Goods goods);

    List<Goods> findHaveInventoryList(int start, Integer rows, Goods goods);

    void saveOrUpdateStock(Integer goodsId, Integer inventoryQuantity, double purchasingPrice);


    void deleteGoodsStock(Integer goodsId);


    Goods findGoodsByGoodsId(Integer goodsId);

    List<Goods> findListAlarm();
}
